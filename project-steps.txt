Project Setup
-------------------------------------------------------------------------
Install Docker and Docker Compose on your local development environment.
Create a new directory for your Rails project.
Navigate to the project directory and create the following files:
Dockerfile: This file defines the Docker image for your Rails application.
docker-compose.yml: This file defines the Docker Compose configuration for your project, including services, volumes, networks, and environment variables.
database.yml: This file defines the configuration for your Rails application to connect to the database using environment variables.
ci-cd.yml: This file defines the CI/CD pipeline for your Rails application, including build, test, and deploy stages.
Open the Dockerfile and define the Docker image for your Rails application. 
Here's an example:
Dockerfile
--------------------------------------------------------------------------------
FROM ruby:2.7.8

# Set the working directory in the container
WORKDIR /app

# Copy the Gemfile and Gemfile.lock into the container
COPY Gemfile Gemfile.lock ./

# Install Bundler and dependencies
RUN gem install bundler:2.3.6
RUN bundle install
RUN bundle exec rails db:create
RUN bundle exec rails db:migrate

# Copy the rest of the application code into the container
COPY . .

# Install Node.js dependencies
RUN apt-get update && apt-get install -y nodejs

# Expose the port on which the application will run
EXPOSE 3000

# Start the Rails application
CMD ["bundle", "exec", "rails", "server", "-b", "0.0.0.0"]
-----------------------------------------------------------------------------
Open the docker-compose.yml file and define the services, volumes, networks, and environment variables for your Rails application. Here's an example:

--------------------------------------------------------------------------------

docker-compose.yaml
-------------------------------------------------------------------------------
version: '3.9'
services:
  web:
    build: .
    ports:
      - "3000:3000"
    volumes:
      - .:/app
    depends_on:
      - db
    env_file:
      - .env  # Specify the path to your .env file
  db:
    image: postgres:14.1
    volumes:
      - ./pgdata:/var/lib/postgresql/data
    environment:
      - POSTGRES_USER=Budgy
      - POSTGRES_PASSWORD=Budgy
      - POSTGRES_DB=budgy_development
------------------------------------------------------------------------------------
Open the database.yml file and configure the Rails application to use environment variables for database connection. Here's an example:
-----------------------------------------------------------------------------------
database.yaml
------------------------------------------------------------------------------------
default: &default
  adapter: postgresql
  encoding: unicode
  pool: <%= ENV.fetch("RAILS_MAX_THREADS") { 5 } %>

development:
  <<: *default
  database: budgy_development
  host: <%= ENV.fetch("DB_HOST") { "db" } %>
  username: <%= ENV.fetch("DB_USERNAME") { "Budgy" } %>
  password: <%= ENV.fetch("DB_PASSWORD") { "Budgy" } %>
  port: <%= ENV.fetch("DB_PORT") { 5432 } %>

test:
  <<: *default
  database: budgy_test
  host: <%= ENV.fetch("DB_HOST") { "db" } %>
  username: <%= ENV.fetch("DB_USERNAME") { "Budgy" } %>
  password: <%= ENV.fetch("DB_PASSWORD") { "Budgy" } %>
  port: <%= ENV.fetch("DB_PORT") { 5432 } %>

production:
  <<: *default
  database: budgy_production
  host: <%= ENV.fetch("DB_HOST") { "db" } %>
  username: <%= ENV.fetch("DB_USERNAME") { "Budgy" } %>
  password: <%= ENV.fetch("DB_PASSWORD") { "Budgy" } %>
  port: <%= ENV.fetch("DB_PORT") { 5432 } %>
----------------------------------------------------------------------------------------------------------------------
Create a CI/CD YAML file (e.g., ci-cd.yml) to define the pipeline for your Rails application. The example below uses a basic pipeline with build, test, and deploy stages:
---------------------------------------------------------------------------------------
pipeline.yaml
----------------------------------------------------------------------------------------
stages:
  - build
  - test
  - deploy

variables:
  DOCKER_COMPOSE_VERSION: 3.9

services:
  - docker:dind

before_script:
  - docker info
  - docker-compose version

build:
  stage: build
  script:
    - docker-compose build
    - docker login -u $DOCKER_USERNAME -p $DOCKER_PASSWORD
    - docker-compose push

test:
  stage: test
  script:
    - docker-compose run --rm app bundle install
    - docker-compose run --rm app bundle exec rails db:create
    - docker-compose run --rm app bundle exec rails db:prepare
    - docker-compose run --rm app bundle exec rspec

deploy:
  stage: deploy
  script:
    - docker-compose up -d
-----------------------------------------------------------------------------------------------
kubernetes installation
---------------------------------------------------------------------------------------------
1  yum install docker -y
    2  systemctl enable docker && systemctl start docker
    3   swapoff -a
    4  vi /etc/docker/daemon.json
    5  ll
    6   sudo systemctl restart docker
    7   sudo systemctl restart docker
    8  cat <<EOF > /etc/yum.repos.d/kubernetes.repo
    9  [kubernetes]
   10  name=Kubernetes
   11  baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
   12  enabled=1
   13  gpgcheck=1
   14  repo_gpgcheck=0
   15  gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
   16  exclude=kube*
   17  EOF
   18  cat <<EOF >  /etc/sysctl.d/k8s.conf
   19  net.bridge.bridge-nf-call-ip6tables = 1
   20  net.bridge.bridge-nf-call-iptables = 1
   21  EOF
   22  sysctl --system
   23  setenforce 0
   24  yum install -y kubelet kubeadm kubectl --disableexcludes=kubernetes
   25  systemctl enable kubelet && systemctl start kubelet
   26  kubeadm init
   27  export KUBECONFIG=/etc/kubernetes/admin.conf
   28  kubectl get nodes
   29  kubectl apply -f https://github.com/weaveworks/weave/releases/download/v2.8.1/weave-daemonset-k8s.yaml
   30  kubectl get nodes
------------------------------------------------------------------------------------------------------------------


